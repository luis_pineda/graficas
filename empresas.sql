-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 30-07-2021 a las 18:00:11
-- Versión del servidor: 10.4.16-MariaDB
-- Versión de PHP: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `empresas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mayoresventas`
--

CREATE TABLE `mayoresventas` (
  `id` int(11) NOT NULL,
  `Fecha` date NOT NULL,
  `Ganancias` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `mayoresventas`
--

INSERT INTO `mayoresventas` (`id`, `Fecha`, `Ganancias`) VALUES
(1, '2021-01-21', 5000),
(2, '2021-02-09', 3500),
(3, '2021-03-02', 10000),
(4, '2021-04-26', 2700.5),
(5, '2021-05-10', 8000),
(6, '2021-06-15', 3000),
(7, '2021-07-12', 4300),
(8, '2021-08-01', 12000),
(9, '2021-09-18', 15000),
(10, '2021-10-27', 7000),
(11, '2021-11-12', 2000),
(12, '2019-06-15', 4000),
(13, '2020-01-03', 7000),
(14, '2019-12-22', 4300),
(15, '2020-06-15', 5000),
(16, '2020-03-24', 8000),
(17, '2019-05-08', 9000),
(18, '2020-05-08', 10000);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `mayoresventas`
--
ALTER TABLE `mayoresventas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `mayoresventas`
--
ALTER TABLE `mayoresventas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
