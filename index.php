<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link type="text/javascript" href="node_modules/chart.js/dist/chart.js">
    <link type="text/javascript" href="node_modules/chart.js/dist/chart.min.js">
    <title>Grafica</title>
    <style>
        .caja{
            margin: auto;
            max-width: 250px;
            padding: 20px;
            border: 1px, solid #BDBDBD;
        }
        .caja select{
            width: 100%;
            font-size: 16px;
            padding: 5px;
        }
        .resultados{
            margin: auto;
            margin-top: 40px;
            width: 2000px;
        }
    </style>
</head>
<body>
    <div class="caja">
        <select onChange="mostrarResultados(this.value);">
            <?php
                for($i = 2019; $i<=2021; $i++)
                {
                    if($i == 2019)
                    {
                        echo '<option value="'.$i.'" selected>'.$i.'</option>';
                    }
                    else
                    {
                        echo '<option value="'.$i.'">'.$i.'</option>';
                    }
                }
            ?>
        </select>
    </div>
    <div class="resultados">
        <canvas id="grafico"></canvas>
    </div>
</body>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>

<script>
    $(document).ready(mostrarResultados(2019));
    function mostrarResultados(ano){
        $.ajax({
            type:'POST',
            url: 'procesar.php',
            data: 'ano='+ano,
            success:function(data){
                var valores = eval(data);

                var ene = valores[0];
                var feb = valores[1];
                var mar = valores[2];
                var abr = valores[3];
                var may = valores[4];
                var jun = valores[5];
                var jul = valores[6];
                var ago = valores[7];
                var sep = valores[8];
                var oct = valores[9];
                var nov = valores[10];
                var dic = valores[11];

                var contexto = document.getElementById('grafico').getContext('2d');
                new Chart(grafico, {
                    type: 'bar',
                    data: {
                        labels : ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                        datasets: [{
                            data: [ene, feb, mar, abr, may, jun, jul, ago, sep, oct, nov, dic],
                            backgroundColor: 'rgba(151,187,205,0.2)',
                            borderColor: 'rgba(151,187,205,1)',
                            pointBackgroundColor: 'rgba(151,187,205,1)'
                        }]
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                beginAtZero: true,
                                stepSize: 10
                                }
                            }]
                        }
                    }
                })
            }
        });

        return false;
    }
</script>
</html>