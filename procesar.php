<?php
    include('conexion.php');

    $ano = $_POST['ano'];

    $enero = mysqli_fetch_array(mysqli_query($mysqli, "SELECT SUM(Ganancias) AS r FROM mayoresventas WHERE MONTH(Fecha)=1 AND YEAR(Fecha)='$ano'"));
    $febrero = mysqli_fetch_array(mysqli_query($mysqli, "SELECT SUM(Ganancias) AS r FROM mayoresventas WHERE MONTH(Fecha)=2 AND YEAR(Fecha)='$ano'"));
    $marzo = mysqli_fetch_array(mysqli_query($mysqli, "SELECT SUM(Ganancias) AS r FROM mayoresventas WHERE MONTH(Fecha)=3 AND YEAR(Fecha)='$ano'"));
    $abril = mysqli_fetch_array(mysqli_query($mysqli, "SELECT SUM(Ganancias) AS r FROM mayoresventas WHERE MONTH(Fecha)=4 AND YEAR(Fecha)='$ano'"));
    $mayo = mysqli_fetch_array(mysqli_query($mysqli, "SELECT SUM(Ganancias) AS r FROM mayoresventas WHERE MONTH(Fecha)=5 AND YEAR(Fecha)='$ano'"));
    $junio = mysqli_fetch_array(mysqli_query($mysqli, "SELECT SUM(Ganancias) AS r FROM mayoresventas WHERE MONTH(Fecha)=6 AND YEAR(Fecha)='$ano'"));
    $julio = mysqli_fetch_array(mysqli_query($mysqli, "SELECT SUM(Ganancias) AS r FROM mayoresventas WHERE MONTH(Fecha)=7 AND YEAR(Fecha)='$ano'"));
    $agosto = mysqli_fetch_array(mysqli_query($mysqli, "SELECT SUM(Ganancias) AS r FROM mayoresventas WHERE MONTH(Fecha)=8 AND YEAR(Fecha)='$ano'"));
    $septiembre = mysqli_fetch_array(mysqli_query($mysqli, "SELECT SUM(Ganancias) AS r FROM mayoresventas WHERE MONTH(Fecha)=9 AND YEAR(Fecha)='$ano'"));
    $octubre = mysqli_fetch_array(mysqli_query($mysqli, "SELECT SUM(Ganancias) AS r FROM mayoresventas WHERE MONTH(Fecha)=10 AND YEAR(Fecha)='$ano'"));
    $noviembre = mysqli_fetch_array(mysqli_query($mysqli, "SELECT SUM(Ganancias) AS r FROM mayoresventas WHERE MONTH(Fecha)=11 AND YEAR(Fecha)='$ano'"));
    $diciembre = mysqli_fetch_array(mysqli_query($mysqli, "SELECT SUM(Ganancias) AS r FROM mayoresventas WHERE MONTH(Fecha)=12 AND YEAR(Fecha)='$ano'"));

    $data = array(0 => round($enero['r'], 1),
        1 => round($febrero['r'], 1),
        2 => round($marzo['r'], 1),
        3 => round($abril['r'], 1),
        4 => round($mayo['r'], 1),
        5 => round($junio['r'], 1),
        6 => round($julio['r'], 1),
        7 => round($agosto['r'], 1),
        8 => round($septiembre['r'], 1),
        9 => round($octubre['r'], 1),
        10 => round($noviembre['r'], 1),
        11 => round($diciembre['r'], 1),
        );
    
    echo json_encode($data);
?>